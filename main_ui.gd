extends Control

export var progress_bar_max_width = 1248

var exit_timer
#var sound_start
#var sound_select

func _ready():
	exit_timer = 0
#	sound_start = load("res://Start.ogg")
#	sound_select = load("res://Select.ogg")

func _process(delta):
	$LabelFPS.text = "FPS: " + str(Engine.get_frames_per_second()) + "\n"
	$LabelFPS.text += "Delta Time: " + str(delta) + "\n"
	if OS.vsync_enabled:
		$LabelFPS.text += "Vsync Enabled"
	else:
		$LabelFPS.text += "Vsync Disabled"
		
	if Input.is_action_pressed("menu"):
		exit_timer += delta
		$ExitProgressBar.rect_size.x += delta * progress_bar_max_width
		if $ExitProgressBar.rect_size.x >= progress_bar_max_width:
			$ExitProgressBar.rect_size.x = progress_bar_max_width
			get_tree().quit()
		
	if Input.is_action_just_released("menu"):
		if exit_timer > 0 and exit_timer < 1:
			OS.vsync_enabled = not OS.vsync_enabled
			$ExitProgressBar.rect_size.x = 0
			exit_timer = 0

func _input(event):
	if event is InputEventKey:
		$LabelKeyPress.text = OS.get_scancode_string(event.get_scancode_with_modifiers()) + " Key Pressed"
		if not event.pressed:
			$AudioStreamPlayer.play()
	elif event is InputEventJoypadButton:
		$LabelKeyPress.text = "Joypad " + str(event.button_index) + " Key Pressed"
#		print(event.button_index)
		if not event.pressed:
			$AudioStreamPlayer.play()
	elif event is InputEventJoypadMotion:
		$LabelKeyPress.text = "Joypad Motion " + str(event.axis) + " (" + str(event.axis_value) + ") " + " Pressed"
