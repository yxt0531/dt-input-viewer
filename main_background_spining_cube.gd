extends Spatial

export var speed = 90

func _process(delta):
	rotation_degrees.y += delta * speed
